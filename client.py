import socket
from socket import *

s = socket(AF_INET,SOCK_STREAM)
host = gethostname()
port = 55600

KEY = "e3_s`[w'JDu"
DATA = bytes.fromhex('095c2d120e7b15422b361c 020801411e4f1111411a0c0b523d5d0e600344787e091c0e084b0d4f5410595f0b5957384d1f7a425b603b134908135712444119580c46415a3f50176d451e713d55421a4053055f4156550a00465b6b0010705f1d76341457015b1d030b4d54581500445d764e01355b1e61711d4e1e4c1d050b405852464841427a0011')


def usage():
    
    print("Encryption functions available are: ")
    print("XORE^ XORB+ XORB-")
    print("1. XORE^[space][key]^[data] ")
    print("2. XORB+[space][key]^[data]")
    print("3. XORB-[space][key]^[data]")

# This function sends some hardcoded values to the servers for testing.
def sendHardCode():
    
    cmd = bytes('XORB+ ' + KEY + '^', encoding='utf8') + DATA
    s.send(cmd)
    d1 = s.recv(1024)
    
    cmd = bytes('XORB- ' + KEY + '^', encoding='utf8') + DATA
    s.send(cmd)
    d2 = s.recv(1024)
    
    cmd = bytes('XORE^ ' + KEY + '^', encoding='utf8') + DATA
    s.send(cmd)
    d3 = s.recv(1024)
    
    print(d1)
    print()
    print(d2)
    print()
    print(d3)
    print()
    
    return


# This function processes the commands for the server   
def processCMD():

    while(1):
        cmd = bytes(input("Enter the command ( u for usage, q to quit) :\n"), encoding='utf8')

        if(cmd.decode() == 'u'):
            usage()
            continue
        elif(cmd.decode() == 'q'):
            return
        elif(cmd.decode() =='h'):
            sendHardCode()
            continue
        elif(cmd.decode() == 'c'):
            #test the vulnerability
            testVulnerableServer()
            #after we send that crash code, we check to see if the server is still alive
            try:     
                s = socket(AF_INET,SOCK_STREAM)
                s.connect((host,port))
            except:
                print("Insecure server is insecure and ran away!")
                return
            else:
                print("Secure server!")
            continue      
        else:
            s.send(cmd)
            rec = s.recv(1024)

        ret = processRecv(rec)

        if( ret == -1):
            print("You sent an invalid command or command is malformed, check your input.")
        else:
            print("Returned data\n"+  rec.decode() )


# This function crashes the vulnerable server due to buffer/integer overflows
def testVulnerableServer():
    #this will crash the vulnerable server
    print("Preparing to crash the server...\n")
    cmd = bytes('XORB- ' + '\xFF'*13 + '^' + '\xFF'*1, encoding='utf8')
    s.send(cmd)
    print(s.recv(1024))
    return      


# This function processes out received data, looks for Bad Message/Type   
def processRecv(rec):
    s = rec.decode()
    #Bad commands return Bad Message, malformed commands return Bad Message Type
    # find the Bad Message or Bad Message Type
    # We'll treat them the same so just find "Bad and Message"
    ret = s.find('Bad Message')

    #if its there we get 0, if not we get -1
    return ret


def main():
    print("Welcome to Client 1.0!\tG Lindor")
    
    usage()
    print()
    s.connect((host,port))
    #Welcome message
    print(s.recv(1024))
    processCMD()


if __name__ == '__main__':
    main()
